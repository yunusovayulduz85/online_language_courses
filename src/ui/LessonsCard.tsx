import { Avatar, Box, Button, ButtonBase, Card, CardActions, CardContent, Typography } from "@mui/material"
import { ItemType } from "../types/lesson"
import { Link, useNavigate } from "react-router-dom"
import { deepOrange } from "@mui/material/colors";
import { Dispatch, SetStateAction, useEffect, useState } from "react";
  type Props = {
    lesson:ItemType,
    // idState:number[],
    // setIdState:Dispatch<SetStateAction<number[]>>
    // setCheckId:Dispatch<SetStateAction<number>>
  }
  const LessonCard:React.FC<Props>  = ({lesson}) => {  
  const quizId = localStorage.getItem("quizId")
  const quizIdNum = Number(quizId)
  const quizLang = localStorage.getItem("quizLang")
  // const score = localStorage.getItem('score')
  // const scoreNum = Number(score)
  // const idState = localStorage.getItem("idState")
  // const idStateArr = idState? JSON.parse(idState) :[]
  // const result = (scoreNum/10)*100;
  const navigation = useNavigate()
  const openGeneralData = () => {
    navigation(`${lesson.id}`)
  }
//   const [idState,setIdState] = useState<number[]>([])
//   const addNewId = (newId:number) => {
//         setIdState(prevIds => [...prevIds,newId])
//  }
//   if(quizIdNum === lesson.id && quizLang ===lesson.lang){
//         addNewId(lesson.id)
//   }
  // console.log(idState)
  // if(quizIdNum === lesson.id && quizLang === lesson.lang){
  //   lesson.score = result
  // }else{
  //  lesson.score = scoreNum
  // }
  return <>
      
        <Card sx={{mt:1,width:"350px",display:'flex',flexDirection:'column',boxShadow:3,background:"#f2e4cf",justifyContent:'space-between'}}>
          <ButtonBase
          onClick={openGeneralData}
          >
             <CardContent>
               <Box sx={{display:'flex',justifyContent:'end'}}>
                  <Typography variant="body2" sx={{color:'#00a152'}}>
                    {
                     (lesson.lang === "eng" && `Appropriation : ${lesson.score !== 0 ? lesson.score+"0%" :"no results"}` || lesson.lang === "rus" && `Aссигнования : ${lesson.score != 0 ? lesson.score+"0%":"Нет результатов"}` || lesson.lang === "uzb" && `O'zlashtirish : ${lesson.score !=0 ? lesson.score+"0%":"Testlar ishlanmagan"}`)
                    }
                    {/* {
                      idState.indexOf(lesson.id) !== -1 && (lesson.lang === "eng" && `Appropriation : ${result}%` || lesson.lang === "rus" && `Aссигнования : ${result}%` || lesson.lang === "uzb" && `O'zlashtirish : ${result}%`)
                    } */}
                  </Typography>
                </Box>
               <Box sx={{display:'flex',justifyContent:'center',mb:"10px"}}>
                    <Avatar sx={{ bgcolor: deepOrange[500] }}>{lesson.id}</Avatar>
                </Box>
               
               <Typography variant='h5' sx={{textAlign:'center',color:"red"}}>
               {lesson.lessonName} 
                </Typography>
               <Typography variant='body1' sx={{textAlign:'center'}}>
                {lesson.description}
                </Typography>
               <Typography variant='h6' sx={{textAlign:'center',color:"#966721"}}>
                 {lesson.level}
                </Typography>
             </CardContent>
          </ButtonBase>
            <CardActions sx={{display:'flex',justifyContent:'center'}}>
              <Link to={`${lesson.id}`}>
              <Button
               variant='contained'
               color='warning'
              >
                 {lesson.lang === "eng" && "Start Lesson"}
                 {lesson.lang === "rus" && "Начать урок"}
                 {lesson.lang === "uzb" && "Darsni boshlash"}
              </Button>
              </Link>
             </CardActions>
         </Card>
  </>
}

export default LessonCard