import { Avatar, Box, Button, Chip, Typography } from '@mui/material'
import React, { useState } from 'react'
import QuestionsCard from './QuestionsCard'
import { fetchQuizQuestions } from '../API';
import {QuestionState, Difficulty } from '../API';
import { useNavigate } from 'react-router';
import WestIcon from '@mui/icons-material/West';
import SportsScoreIcon from '@mui/icons-material/SportsScore';
import DoubleArrowIcon from '@mui/icons-material/DoubleArrow';
import CreditScoreIcon from '@mui/icons-material/CreditScore';
export type AnswerObject = {
    question:string,
    answer:string,
    correct:boolean,
    correctAnswer:string
}

const TOTAL_QUESTIONS = 10;
const lang = localStorage.getItem("language")
const QuizTest = () => {

    const [loading,setLoading] = useState(false)
    const [questions,setQuestions] = useState<QuestionState[]>([])
    const [number,setNumber] = useState(0)
    const [userAnswers,setUserAnswers] = useState<AnswerObject[]>([])
    const [score,setScore] = useState(0)
    const [gameOver,setGameOver] = useState(true)
    localStorage.setItem("score",score.toString())

    ///////////////////////////////////////
    const quizId = localStorage.getItem("quizId")
    const quizIdNum = quizId ? JSON.parse(quizId):null
if(lang == "Eng"){

    const english  = localStorage.getItem("english")
    const englishArr= english ? JSON.parse(english):[] 
    
    for (let i = 0; i < englishArr.length; i++) {
        if(englishArr[i].id == quizIdNum){ 
            englishArr[i].score = score 
        }
    }
    localStorage.setItem("english",JSON.stringify(englishArr))
}  
if(lang == "Rus"){
    console.log("rus shartini ichiga kirdi");
    const russian  = localStorage.getItem("russian")
    const russianArr = russian ? JSON.parse(russian):[] 
    
    for (let i = 0; i < russianArr.length; i++) {
        if(russianArr[i].id == quizIdNum){ 
            russianArr[i].score = score 
        }
    }
    console.log(russianArr)
    localStorage.setItem("russian",JSON.stringify(russianArr))
}
 if(lang == "Uzb"){
     const uzbek  = localStorage.getItem("uzbek")
    const uzbekArr= uzbek ? JSON.parse(uzbek):[] 
    
    for (let i = 0; i < uzbekArr.length; i++) {
        if(uzbekArr[i].id == quizIdNum){ 
            uzbekArr[i].score = score 
        }
    }
    localStorage.setItem("uzbek",JSON.stringify(uzbekArr))
}




    const navigation = useNavigate()
    const startTrivia = async () => {
    setLoading(true);
    setGameOver(false);
    const newQuestions = await fetchQuizQuestions(
        TOTAL_QUESTIONS,
        Difficulty.MULTIPLE
    )
    setQuestions(newQuestions)
    setScore(0)
    setUserAnswers([])
    setNumber(0)
    setLoading(false)
  }

  const checkAnswer = (e:React.MouseEvent<HTMLButtonElement>) => {
    if(!gameOver){
        //*tanlangan javobni ushlab oldim
        const answer = e.currentTarget.value;
        //*tanlangan javob bilan to'g'ri javobni solishtirdim
        const correct = questions[number].correct_answer === answer;
        //*to'g'ri javob tanlansa bal qo'shiladi
        if(correct) setScore(prev => prev +1)
        const answerObject = {
            question:questions[number].question,
            answer,
            correct,
            correctAnswer:questions[number].correct_answer,

        }
        setUserAnswers((prev) => [...prev,answerObject])
        
    }
  }

  const nextQuestion = () => {
    const nextQuestion = number + 1;
    if(nextQuestion === TOTAL_QUESTIONS){
        setGameOver(true)
    }else{
        setNumber(nextQuestion)
    }
  }

  const stopQuestion = () => {
    setGameOver(true)
  }

  return (
    <Box sx={{display:'flex',justifyContent:'center',maxWidth:'screen',minHeight:'100vh',background:'#f6dbb3'}}>
         <Box sx={{mt:2}}>
        <Typography variant='h3' sx={{textAlign:'center',fontWeight:'bold'}}>
            {
                lang === "Eng" && "The Quiz Tests"
            }
            {
                lang === "Rus" && "Тесты-викторины"
            }
            {
                lang === "Uzb" && "Viktorina testlari"
            }
            
        </Typography>
        <Typography variant='body1' sx={{textAlign:'center',fontWeight:'bold',mt:2}}>
             {
                lang === "Eng" && "10 questions to test your mastery"
            }
            {
                lang === "Rus" && "10 вопросов, чтобы проверить свое мастерство"
            }
            {
                lang === "Uzb" && "Sizning mahoratingizni tekshirish uchun 10 ta savol"
            }
            
        </Typography>
        <Box sx={{display:'flex',justifyContent:'center',mt:2}}>
             {
            gameOver || userAnswers.length === TOTAL_QUESTIONS ? ( <Box sx={{display:'flex',gap:'6px'}}>
            <Button onClick={() => navigation(-2)} variant='contained' color='warning'>
                <WestIcon/>
                 {
                    lang === "Eng" && "Back"
                }
                {
                    lang === "Rus" && "Назад"
                }
                {
                    lang === "Uzb" && "Orqaga"
                }
                
            </Button>
            <Button onClick={startTrivia} variant='contained' color='warning'>
                {
                    lang === "Eng" && "Start"
                }
                {
                    lang === "Rus" && "Начинать"
                }
                {
                    lang === "Uzb" && "Boshlash"
                }
            
            <SportsScoreIcon/>
            </Button>
       </Box>
        ):null
        }
        </Box>
       {
        !gameOver &&  <Box sx={{display:'flex',justifyContent:'center',mb:1,mt:2}}>
             <Chip 
                label= { lang === "Eng" && `Number of correct answers : ${score}` || lang === "Rus" && `Количество правильных ответов : ${score}` || lang === "Uzb" && `To'g'ri javoblar soni : ${score}` }
                color='warning'
                size='medium'
                variant='outlined'
                avatar={<Avatar sx={{backgroundColor:'#dd8b10',p:0.2}}><CreditScoreIcon sx={{color:'white'}}/></Avatar>}
                />
            </Box>
       
       }       
        {loading && <Typography variant='body1'sx={{textAlign:'center',fontWeight:'bold'}}>Loading Questions ...</Typography>}
        {!loading && !gameOver && (
        <QuestionsCard
        questionNr={number+1}
        totalQuestions={TOTAL_QUESTIONS}
        question={questions[number].question}
        answers={questions[number].answer}
        userAnswer={userAnswers ? userAnswers[number] : undefined }
        callback={checkAnswer}
        correct={userAnswers[number]?.correct}
        userClicked={userAnswers[number]?.answer!==""}
        />)}
        <Box sx={{display:'flex',justifyContent:'center',gap:1,mt:1}}>
        {
             !gameOver && !loading && number !== TOTAL_QUESTIONS-1 && <Button onClick={stopQuestion} variant='contained'  sx={{backgroundColor:"#ff3d00",'&:hover':{backgroundColor:'#f46033'}}}>
                {
                    lang === "Eng" && "Stop"
                }
                {
                    lang === "Rus" && "Стоп"
                }
                {
                    lang === "Uzb" && "Tugatish"
                }
                
            </Button>
        }
         
        {
            !gameOver && !loading && userAnswers.length === number +1 &&
            number !== TOTAL_QUESTIONS - 1 ?(
                <Button onClick={nextQuestion} variant='contained' color='warning'>
                {
                    lang === "Eng" && "Next Question"
                }
                {
                    lang === "Rus" && "Следующий вопрос"
                }
                {
                    lang === "Uzb" && "Keyingi savol"
                }
                    
                    <DoubleArrowIcon />
                </Button>
            ):null
        }
        </Box>
       
    </Box>
    </Box>
   
  )
}

export default QuizTest