import { Box, Button, Typography } from "@mui/material"
import React from "react"
import { AnswerObject } from "./QuizTest"

type Props = {
    question:string,
    answers:string[],
    callback:(e:React.MouseEvent<HTMLButtonElement>) => void,
    userAnswer:AnswerObject | undefined,
    questionNr:number,
    totalQuestions:number,
    correct:boolean,
    userClicked:boolean
}
const lang = localStorage.getItem("language")
const QuestionsCard:React.FC<Props> = ({
    question,
    answers,
    callback,
    userAnswer,
    questionNr,
    totalQuestions,
}) => {
  return (
    <>
        <Typography variant="body1" sx={{textAlign:'center',fontWeight:'bold'}}>
           {
            lang === "Eng" && `Question :${questionNr} / ${totalQuestions}`
           }  
           {
            lang === "Rus" && `Вопрос :${questionNr} / ${totalQuestions}`
           }  
           {
            lang === "Uzb" && `Savol :${questionNr} / ${totalQuestions}`
           }  
        </Typography>
        <Typography  variant="h5" sx={{textAlign:'center',my:1,fontWeight:'bold'}}>{question}</Typography>
        <Box sx={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'center'}}>
            {
                answers?.map(answer => (
                    <Box key={answer} >
                        <Button 
                        disabled={userAnswer ? true : false} 
                        value={answer} 
                        onClick={callback}
                        // variant="contained"
                        sx={{
                            mt:1,
                            color:'white',
                            width:"500px",
                            backgroundColor:userAnswer?.answer === answer ? (userAnswer?.correctAnswer == answer ? "#438f46":"#ff3d00"):(userAnswer?.correctAnswer === answer ? "#438f46":"#ff9800"),
                            '&:hover':{
                                backgroundColor:"#ffac33"
                            }
                        }}
                        >
                            <Typography>{answer}</Typography>
                        </Button>
                    </Box>
                ))
            }
        </Box>
    </>
  )
}

export default QuestionsCard