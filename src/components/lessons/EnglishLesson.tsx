import React, { useState } from 'react'
import { Skeleton, Typography} from '@mui/material'
import LessonCard from '../../ui/LessonsCard'
import { LessonCardWrapper } from '../../styles/LessonCart.modules'
import { ItemType } from '../../types/lesson'
const EnglishLesson:React.FC = () => {
  const getEnglishLesson = localStorage.getItem("english")
  const getEnglishLessonArr = getEnglishLesson? JSON.parse(getEnglishLesson):[]
  const quizId = localStorage.getItem("quizId")
  const quizIdNum = Number(quizId)
  const quizLang = localStorage.getItem("quizLang")
  // const [idState,setIdState] = useState<number[]>([])
//   const addNewId = (newId:number) => {
//         setIdState(prevIds => [...prevIds,newId])
//  }
 
  // console.log(idState)
  return <>
     <Typography variant="h3" sx={{textAlign:'center',mt:"-50px",mb:2,color:'#ff9800'}}>
         All Lessons
      </Typography>
  <LessonCardWrapper>
    
        {
        getEnglishLessonArr?.map((item:ItemType , index:number) => {
        //  if(quizIdNum === item.id && quizLang === item.lang){
        //     item.score 
        // }

        return<>
        <LessonCard 
          lesson={item}
          key={index}
          // idState={idState}
          />
        </>
         })
        }
      </LessonCardWrapper>   
  </>
   
}

export default EnglishLesson