import { LessonCardWrapper } from '../../styles/LessonCart.modules'
import { Typography } from '@mui/material'
import { ItemType } from '../../types/lesson'
import LessonCard from '../../ui/LessonsCard'
import { useState } from 'react'
const UzbekLesson = () => {
  const getUzbekLesson = localStorage.getItem("uzbek")
  const getUzbekLessonArr = getUzbekLesson? JSON.parse(getUzbekLesson):[]
  const quizId = localStorage.getItem("quizId")
//   const quizIdNum = Number(quizId)
//   const quizLang = localStorage.getItem("quizLang")
//    const [idState,setIdState] = useState<number[]>([])
//   const addNewId = (newId:number) => {
//         setIdState(prevIds => [...prevIds,newId])
//  }
  return (
    <>
     <Typography variant="h3" sx={{textAlign:'center',mt:"-50px",mb:2,color:'#ff9800'}}>
         Barcha Darslar
      </Typography>
    <LessonCardWrapper>
      {
       getUzbekLessonArr?.map((item:ItemType , index:number) => {
        // if(quizIdNum === item.id && quizLang === item.lang){
        //      addNewId(item.id)
        //  }
        return<>
        {
         <LessonCard 
          lesson={item}
          key={index}
          // idState={idState}
          />
        }
          
        </>
         })
        }
     </LessonCardWrapper>
    </>
     
  )
}

export default UzbekLesson