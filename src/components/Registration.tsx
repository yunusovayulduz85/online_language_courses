import { Box, Button, Grid, Stack, TextField, Typography } from "@mui/material"
import { useNavigate } from "react-router-dom"
import { orange } from "@mui/material/colors"
import Image from "../assets/image.jpg"
import { useState } from "react"

const CreateAccount = () => {

  const [name, setName] = useState<string>("")
  const [email, setEmail] = useState<string>("")
  const [password, setPassword] = useState<number>(0)
  const navigation = useNavigate();
  const handleClick = () => {    
    if(isValidate()){
      localStorage.setItem("name",name)
      localStorage.setItem("email",email)
      localStorage.setItem("password",password.toString())
      alert('Registered Successfully!')
      navigation('/Login')
    }
    
  }

   const isValidate = () => {
    let isProceed = true;
    let errMessage = "Please enter the value in "
    if(name === "" ||name === null){
      isProceed = false;
      errMessage += 'name'
    }
    else if(email === "" || email === null){
      isProceed = false
      errMessage += 'email'
    }
    else if(!email.includes('@')){
      isProceed = false
      errMessage = "You entered the wrong email!"
    }
    else if(!password){
      isProceed = false
      errMessage +='password'
    }
    return isProceed;
  }

  return (
    <Box sx={{display:'flex',justifyContent:'center',alignItems:'center', mt:10, gap:"0px"}}>
      <Grid container rowSpacing={0} sx={{width:"1000px",bgcolor:'white',paddingX:'40px',paddingY:"30px",borderRadius:"15px",boxShadow:2,justifyContent:'center',alignItems:'center'}}>
        <Grid item xs={5}>
         <img width={"90%"} style={{borderTopLeftRadius:'15px',borderBottomLeftRadius:'15px',display:'flex',justifyContent:'end'}} src={Image} alt="learning_language"/>
        </Grid>
            <Grid item xs={7}>
               <Stack spacing={2}>
                    <Typography variant="h4"  color={orange[500]}>User Registration</Typography>
                    <TextField  label='Name..' variant='outlined' color="warning" value={name} onChange={(e) => setName(e.target.value)}/>
                    <TextField label='Email..' variant='outlined' color="warning" value={email} onChange={(e) => setEmail(e.target.value)}/>
                    <TextField label='Password..' type="password" variant='outlined' color="warning"  onChange={(e) => setPassword(Number(e.target.value))}/>
                      <Box sx={{display:"flex",justifyContent:"end",gap:'5px'}}>
                         <Button onClick={handleClick}  variant='contained' color='warning'>Register</Button>
                         <Button onClick={() => navigation('/Login')}  variant='contained' color='warning'>Sign in</Button>
                       </Box>
                 </Stack>
             </Grid>     
      </Grid>
       
       
    </Box>
  )
}

export default CreateAccount