import { ItemType } from "../types/lesson";
import { Avatar, Box, Button, Card, CardActions, CardContent, Typography } from "@mui/material";
import { deepOrange } from "@mui/material/colors";
import { useState } from "react";
import { useNavigate } from "react-router";

const DetailLessonsCard = (item:ItemType) => {
 const navigation = useNavigate()
 
 const NavigateQuizPage = () => {
     localStorage.setItem('quizId',item.id.toString())
     localStorage.setItem("quizLang",item.lang)
     navigation('/quizTests')
     window.location.reload()
}
  return   <>
             <Box sx={{display:'flex',justifyContent:'center',mt:"-35px"}} >
                    <Card sx={{width:"500px",background:'#f3ece1',boxShadow:3}}>
                        <CardContent>
                            <Box sx={{display:'flex',justifyContent:'center',mb:"10px"}}>
                               <Avatar sx={{ bgcolor: deepOrange[500] }}>{item.id}</Avatar>
                            </Box>
                        <Typography variant='h6' sx={{textAlign:'center'}}>
                          {item.lessonName}
                        </Typography>
                        <Typography variant='h4' sx={{textAlign:'center'}}>
                            {item.lang === "eng"&& 'General informations'}
                            {item.lang === "rus" && 'Общая информация'}
                            {item.lang === "uzb" && 'Umumiy Ma\'lumotlar'}
                        </Typography>
                        <Typography variant='body1' sx={{textAlign:'justify'}}>{item.data}</Typography>
                        </CardContent>
                        <CardActions sx={{display:'flex',justifyContent:'center',alignItems:'center'}}>
                            <Button  variant='contained' color='warning' onClick={() => navigation(-1)}>
                                {item.lang === "eng" && "Back"}
                                {item.lang === "rus" && "Назад"}
                                {item.lang === "uzb"&& "Orqaga"}
                            </Button>
                            <Button  variant='contained' color='warning' onClick={NavigateQuizPage}>
                                {item.lang === "eng" && "Try yourself"}
                                {item.lang === "rus" && "Попробуйте себя"}
                                {item.lang === "uzb"&& "O'zingizni sinab ko'ring"}
                            </Button>
                        </CardActions>
                    </Card>
                </Box>
               

     </>

  
  
}

export default DetailLessonsCard